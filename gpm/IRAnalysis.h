#include "IRAnalysis_Base.h"
#include "IR_Features.h"
#include "llvm/Constant.h"

using namespace llvm;
using std::ios;
using std::cout;
using std::endl;

typedef std::map<FunctionStats,float > FuncFeatureMap;
typedef std::map<LoopStats,float > LoopFeatureMap;

#define MAPINSERT(X,Y) thisFuncMap.insert(std::pair<FunctionStats,float>(X,Y));

namespace {

struct LoopAnalysis: public LoopPass {
    
};
  
struct IRAnalysis: public ModulePass {

    std::vector<FuncFeatureMap> funcFeatures;

    static char ID;
    IRAnalysis(): ModulePass(ID) {}

    void printFeatureMap(FuncFeatureMap& thisFuncMap)
    {
        std::ofstream outFile;
        funcFeatures.push_back(thisFuncMap);
        outFile.open("Stats.txt",ios::app|ios::out);
        FuncFeatureMap::iterator it;
        for (it = thisFuncMap.begin(); it != thisFuncMap.end(); it++)
        {
            outFile << it->first << " " << getFeatureName(it->first) << " " << it->second << std::endl;
        }
        outFile << std::endl;
        outFile.close();
        return;
    }

    void getSizeDataForAllBB(Function* F, FuncFeatureMap& thisFuncMap)
    {

        int numSmallBB =0;
        int numMedBB =0;
        int numLargeBB = 0;
        for (Function::iterator i = F->begin(), end = F->end(); i != end; ++i)
        {
            if (i->size() <= 15)
                numSmallBB++;
            else if (i->size() <= 500)
                numMedBB++;
            else numLargeBB++;
        }
        MAPINSERT(NUM_SMALL_BB, numSmallBB);
        MAPINSERT(NUM_MEDIUM_BB, numMedBB);
        MAPINSERT(NUM_LARGE_BB, numLargeBB);
        return;
    }


    void getSuccPredDataForAllBB(Function* F, FuncFeatureMap& thisFuncMap)
    {
        int numBB = F->size();
        int numSucc = 0;
        int numPred = 0;
        float numAvgSucc = 0;
        float numAvgPred =0;
        for (Function::iterator i = F->begin(), end = F->end(); i != end; ++i)
        {
            BasicBlock* BB = i;
            for (succ_iterator SI = succ_begin(BB), E = succ_end(BB); SI != E; ++SI)
            {
                // Ensures that we count all the successors for all BBs in
                // the function
                numSucc++;
            }

            for (pred_iterator PI = pred_begin(BB), E = pred_end(BB); PI != E; ++PI)
            {
                // Ensures that we count all the predecessors for all BBs in
                // the function
                numPred++;
            }
        }
        if (numBB == 0)
        {

            MAPINSERT(NUM_AVG_BB_SUCCESSORS, 0.0);
            MAPINSERT(NUM_AVG_BB_PREDECESSORS,0.0);
        }
        else
        {
            numAvgSucc = numSucc/numBB;
            numAvgPred = numPred/numBB;
            MAPINSERT(NUM_AVG_BB_SUCCESSORS, numAvgSucc);
            MAPINSERT(NUM_AVG_BB_PREDECESSORS,numAvgPred);
        }
        return;
    }

    void getOperandMixData(Function*F, FuncFeatureMap& thisFuncMap)
    {
        BasicBlock* BB;
        Instruction* I;
        Value* Op;
        BasicBlock::iterator BI;
        int numOperands=0, ctr=0;
        int numOccurencesOf0=0, numOccurencesOf1=0, numOccurencesOfInt=0;
        int numLocalVarsReferenced=0;
        Argument* Arg;
        GlobalVariable* GVar;
        GetElementPtrInst* GE;

        for (Function::iterator i = F->begin(), end = F->end(); i != end; i++)
        {
            BB = i;
            for (BI = BB->begin(); BI != BB->end(); BI++)
            {
                I = BI;
                numOperands = I->getNumOperands();
                for (ctr=0; ctr<numOperands; ctr++)
                {
                    Op = I->getOperand(ctr);
                    // If its a constant, look for 0 and 1.
                    if (ConstantInt* CInt = dyn_cast<ConstantInt>(Op))
                    {
                        if (CInt->getValue() == 0 and !(GE = dyn_cast<GetElementPtrInst>(I)))
                        {
                            cout << "Got value 0: Opcode is " << I->getOpcodeName() << endl;
                            numOccurencesOf0++;
                        }
                        else if (CInt->getValue() == 1)
                            numOccurencesOf1++;
                    }
//                    // If not a constant and not a global var and not a func.
//                    // argument, then it should be a local var. Thats the
//                    // assumption atleast.
//                    else if (!( (Arg = dyn_cast<Argument>(Op)) || (GVar =dyn_cast<GlobalVariable>(Op)) ) )
//                    {
//                        std::cout << Op->getNameStr() << " is a local variable" << std::endl;
//                        numLocalVarsReferenced++;
//                    }

                    if (Op->getType()->isIntegerTy())
                        numOccurencesOfInt++;
                }

            }

        }
        MAPINSERT(NUM_OCCURENCES_OF_0, numOccurencesOf0);
        MAPINSERT(NUM_OCCURENCES_OF_1, numOccurencesOf1);
        MAPINSERT(NUM_OCCURENCES_OF_INT, numOccurencesOfInt);
        return;
    }

    void getInstnMixData(Function* F, FuncFeatureMap& thisFuncMap)
    {
        int numBB = 0;
        float  numAvgInstnsPerBB =0, numAvgPHINodesPerBB = 0;
        int numInstns = 0, numUnaryInstns =0, numBinaryIntOps = 0, numBinaryFPOps = 0, numSwitchInsts=0;
        int numDirectCalls =0, numIndirectCalls =0, numCondBranches=0, numUncondBranches=0;
        int numPointerArithOps = 0, numPHINodes =0, numPHINodeOperands=0;
        int numBinaryOpsWithOneConstOp =0, numFuncCallsWithMoreThan4Args=0, numFuncCallsWithPtrArgs=0;
        int numFuncCallsReturningPtr =0, numFuncCallsReturningInt =0;

        for (Function::iterator i = F->begin(), end = F->end(); i != end; ++i)
        {
            BasicBlock* BB = i;
            numBB++;
            numInstns += BB->size();
            BasicBlock::iterator BI;
            for (BI = BB->begin(); BI != BB->end(); BI++)
            {
                Instruction* I = BI;
                if (UnaryInstruction* UI = dyn_cast<UnaryInstruction>(I))
                {
                    numUnaryInstns++;
                    if (UI->getOperand(0)->getType()->isPointerTy())
                        numPointerArithOps++;
                }

                if (I->isBinaryOp())
                {
                    Value* op0 = I->getOperand(0);
                    Value* op1 = I->getOperand(1);
                    const Type* op0_Type = op0->getType();
                    const Type* op1_Type  = op1->getType();
                    Constant* CLHS;
                    Constant* CRHS;

                    if ( op0_Type->isIntegerTy() &&  op1_Type->isIntegerTy() )
                        numBinaryIntOps++;
                    if ( op0_Type->isFloatTy() &&  op1_Type->isFloatTy() )
                        numBinaryFPOps++;
                    if ((CLHS = dyn_cast<Constant>(op0)) || (CRHS = dyn_cast<Constant>(op1)) )
                        numBinaryOpsWithOneConstOp++;
                }

                if (CallInst* CI = dyn_cast<CallInst>(I))
                {
                    // Null is returned if it is an indirect call
                    if (CI->getCalledFunction() != NULL)
                    {
                        numDirectCalls++;
                        Function* F = CI->getCalledFunction();
                        if (F->getReturnType()->isPointerTy())
                            numFuncCallsReturningPtr++;
                        else if (F->getReturnType()->isIntegerTy())
                            numFuncCallsReturningInt++;
                    }
                    else
                        numIndirectCalls++;
                    // Get info about the func call arguments
                    unsigned int numArgs = CI->getNumArgOperands();
                    unsigned int ctr=0;
                    if (numArgs > 4)
                        numFuncCallsWithMoreThan4Args++;
                    for (ctr=0; ctr<numArgs; ctr++)
                    {
                        if (CI->getArgOperand(ctr)->getType()->isPointerTy())
                        {
                            numFuncCallsWithPtrArgs++;
                            break;
                        }
                    }

                }

                if (BranchInst* BI = dyn_cast<BranchInst>(I))
                {
                    if (BI->isUnconditional())
                        numUncondBranches++;
                    else if (BI->isConditional())
                        numCondBranches++;
                }

                if (SwitchInst* SI = dyn_cast<SwitchInst>(I) )
                {
                    numSwitchInsts++;
                }

                if (I->isBinaryOp() && (I->getOperand(0)->getType()->isPointerTy() || I->getOperand(1)->getType()->isPointerTy()  ))
                {
                    numPointerArithOps++;
                }


                if (PHINode* Phi= dyn_cast<PHINode>(I))
                {
                    numPHINodes++;
                    numPHINodeOperands += Phi->getNumOperands();
                }
            }

        }
        if (numBB != 0)
        {
            numAvgInstnsPerBB = numInstns/(float) numBB;
            numAvgPHINodesPerBB = numPHINodes/ (float) numBB;
            MAPINSERT(NUM_AVG_INSTNS_PER_BB, numAvgInstnsPerBB);
            MAPINSERT(NUM_AVG_PHI_NODES_PER_BB, numAvgPHINodesPerBB);
        }
        else
        {
            MAPINSERT(NUM_AVG_INSTNS_PER_BB, 0.0);
            MAPINSERT(NUM_AVG_PHI_NODES_PER_BB, 0.0);
        }
        MAPINSERT(NUM_INSTNS,numInstns);
        MAPINSERT(NUM_UNARY_INSTNS,numUnaryInstns);
        MAPINSERT(NUM_BINARY_INT_OPS,numBinaryIntOps);
        MAPINSERT(NUM_BINARY_FP_OPS,numBinaryFPOps);
        MAPINSERT(NUM_DIRECT_CALLS,numDirectCalls);
        MAPINSERT(NUM_INDIRECT_CALLS,numIndirectCalls);
        MAPINSERT(NUM_CONDITIONAL_BRANCHES,numCondBranches);
        MAPINSERT(NUM_UNCONDITIONAL_BRANCHES,numUncondBranches);
        MAPINSERT(NUM_POINTER_ARITH_INSTNS,numPointerArithOps);
        MAPINSERT(NUM_PHI_NODES,numPHINodes);
        MAPINSERT(NUM_SWITCH_INSTNS, numSwitchInsts);
        MAPINSERT(NUM_AVG_ARGS_PER_PHI_NODE, ((float)numPHINodeOperands/numPHINodes));
        MAPINSERT(NUM_BINARY_OPS_WITH_ONE_OP_CONST, numBinaryOpsWithOneConstOp);
        MAPINSERT(NUM_FUNC_CALLS_WITH_POINTER_ARGS,numFuncCallsWithPtrArgs);
        MAPINSERT(NUM_FUNC_CALLS_WITH_MORE_THAN_4_ARGS,numFuncCallsWithMoreThan4Args);
        MAPINSERT(NUM_FUNC_CALLS_RETURNING_POINTER,numFuncCallsReturningPtr);
        MAPINSERT(NUM_FUNC_CALLS_RETURNING_INT, numFuncCallsReturningInt);
        return;

    }

    void addFuncFeaturesToMap(Function* F)
    {
        // This is technically wrong, but I can't help. Function calls to
        // shared library functions also are counted as "function" in the
        // module's list. I need to avoid those as they give 0 for all stats.
        if (F->isDeclaration())
        {
            return;
        }


        FuncFeatureMap thisFuncMap;

        // Get the number of basic blocks in the functions
        MAPINSERT(NUM_BB, F->size());

        // Get successor and predecessor statistics
        getSuccPredDataForAllBB(F,thisFuncMap);

        // Get the size of each basic block, and classify it
        getSizeDataForAllBB(F, thisFuncMap);

        // Get # of edges in the CFG of the function

        // Classify the types of edges in the CFG of the function

        // Number of instructions in the function and the instruction mix
        // data. Unary, binary, int or fp, direct/indirect function calls etc
        getInstnMixData(F,thisFuncMap);

        getOperandMixData(F,thisFuncMap);

        // Print the feature map for this function
        printFeatureMap(thisFuncMap);

        return;
    }

    virtual bool runOnModule(Module& M)
    {
        // Need to structure the data gathering, order the different
        // functions for gathering different fields in the "feature map"
        std::ofstream outFile;
        outFile.open("Stats.txt",ios::app|ios::out);
        Module::iterator i;
        for (i=M.begin(); i !=M.end(); i++)
        {
            if (!(i->isDeclaration()))
                outFile << "Function: " << i->getNameStr() << std::endl;
            addFuncFeaturesToMap(i);
        }
        outFile.close();
        return true;
    }
};
char IRAnalysis::ID =0;
static RegisterPass<IRAnalysis>Z("analyse_ir","Routines to analyse the IR", false, false);
}

