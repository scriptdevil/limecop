#include <string>

/* For the passes */
#include "llvm/LinkAllPasses.h"

/* For PassKind, Pass and PassInfo  */
#include "llvm/Pass.h"
#include "llvm/PassSupport.h"
/* For PassRegistry */
#include "llvm/PassRegistry.h"


class GPMPassRegistrationListener : public llvm::PassRegistrationListener
{
private:
  std::string currentName;
  const llvm::PassInfo *currentPassInfo;
public:
  std::set<std::string> setOfAllPasses; 
  GPMPassRegistrationListener()
    : currentPassInfo(NULL)
  {}
  void getPassNames(){
    enumeratePasses();
  }
  const llvm::PassInfo* getPassInfoForName(std::string name){
    currentName = name;
    currentPassInfo = NULL;
    
    enumeratePasses();
    return currentPassInfo;
  }
  virtual void passEnumerate(const llvm::PassInfo *p){
      if(currentName == NULL){
         setOfAllPasses.insert(p->getPassArgument());
      }
      else{
    if((currentPassInfo == NULL) && (currentName.compare(p->getPassArgument()) == 0)){
      this->currentPassInfo = p;
    }
  }
}
};
