/* For tool_output_file */
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/ToolOutputFile.h"

/* For OwningPtr */
#include "llvm/ADT/OwningPtr.h"

/* For createPrintModulePass */
#include "llvm/LinkAllVMCore.h"

/* For LLVMContext*/
#include "llvm/LLVMContext.h"
/* For PassManager */
#include "llvm/PassManager.h"
/* For PassRegistry */
#include "llvm/PassRegistry.h"
/* For Module */
#include "llvm/Module.h"

/* Standard Includes */
#include <string>
#include <vector>

#include "GPMPassRegistrationListener.hpp"

class GPM
{
public:
  GPM(std::string inFileName, std::string outFileName); 
  bool runForPasses(std::vector<std::string> passNames);
  std::auto_ptr<llvm::Module> M;
  void dumpToFile();
private:
  GPMPassRegistrationListener *gpmprl_;
  std::string inFileName_;
  std::string outFileName_;
  llvm::LLVMContext *Context_;
  llvm::PassRegistry *Registry_;
  llvm::OwningPtr<llvm::tool_output_file> Out_;
  llvm::TargetData *TD_;
  llvm::PassManager *Passes_;
  /* llvm::llvm_shutdown_obj y; */
};
