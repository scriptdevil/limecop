// File containing the enums for features of the IR

#ifndef __FEATURES_H
#define __FEATURES_H

#include <cstdlib>
#include <iostream>

using std::string;

enum LoopStats {
    
};

enum FunctionStats {
    // Basic Block level statistics.
    NUM_BB=0,
    NUM_AVG_BB_SUCCESSORS,
    NUM_AVG_BB_PREDECESSORS,
    NUM_SMALL_BB,
    NUM_MEDIUM_BB,
    NUM_LARGE_BB,

    // CFG Based Statistics
    NUM_EDGES_IN_CFG,
    NUM_CRITICAL_EDGES_IN_CFG,
    NUM_ABNORMAL_EDGES_IN_CFG,

    // Instruction Mix based Statistics
    NUM_INSTNS,
    NUM_UNARY_INSTNS,
    NUM_BINARY_INT_OPS,
    NUM_BINARY_FP_OPS,
    NUM_DIRECT_CALLS,
    NUM_INDIRECT_CALLS,
    NUM_CONDITIONAL_BRANCHES,
    NUM_UNCONDITIONAL_BRANCHES,
    NUM_SWITCH_INSTNS,
    NUM_POINTER_ARITH_INSTNS,
    // NUM_ASSIGNMENT_INSTNS, // Doesn't seem to make sense
    NUM_PHI_NODES,
    NUM_AVG_INSTNS_PER_BB,
    NUM_AVG_PHI_NODES_PER_BB,
    NUM_AVG_ARGS_PER_PHI_NODE,
    // NUM_POINTER_INDIRECT_REFS, // How do I count these?
    // NUM_AVG_USE_OF_ADDR_OP_FOR_VAR,
    // NUM_AVG_USE_OF_ADDR_OP_FOR_FUNC,
    NUM_BINARY_OPS_WITH_ONE_OP_CONST,
    NUM_FUNC_CALLS_WITH_POINTER_ARGS,
    NUM_FUNC_CALLS_WITH_MORE_THAN_4_ARGS,
    NUM_FUNC_CALLS_RETURNING_POINTER,
    NUM_FUNC_CALLS_RETURNING_INT,

    // Argument/Variable Analysis in the Function.
    NUM_OCCURENCES_OF_0,
    NUM_OCCURENCES_OF_1,
    NUM_OCCURENCES_OF_INT,
    // NUM_LOCAL_VARS_REFERENCED,
    // NUM_EXTERN_STATIC_VARS_REFERENCED,
    // NUM_LOCAL_VARS_THAT_ARE_POINTERS,
    // NUM_EXTERN_STATIC_VARS_THAT_ARE_POINTERS

    // TODO: Dependency level analysis. How many instructions
    // in the function are dependent on other instructions
    // in the same function.
};

string getFeatureName(FunctionStats feature)
{
    string featureName;
    switch (feature)
    {
    case NUM_BB:
        featureName.assign("NUM_BB");
        return featureName;
    case NUM_AVG_BB_SUCCESSORS:
        featureName.assign("NUM_AVG_BB_SUCCESSORS");
        return featureName;
    case NUM_AVG_BB_PREDECESSORS:
        featureName.assign("NUM_AVG_BB_PREDECESSORS");
        return featureName;
    case NUM_SMALL_BB:
        featureName.assign("NUM_SMALL_BB");
        return featureName;
    case NUM_MEDIUM_BB:
        featureName.assign("NUM_MEDIUM_BB");
        return featureName;
    case NUM_LARGE_BB:
        featureName.assign("NUM_LARGE_BB");
        return featureName;
    case NUM_EDGES_IN_CFG:
        featureName.assign("NUM_EDGES_IN_CFG");
        return featureName;
    case NUM_CRITICAL_EDGES_IN_CFG:
        featureName.assign("NUM_CRITICAL_EDGES_IN_CFG");
        return featureName;
    case NUM_ABNORMAL_EDGES_IN_CFG:
        featureName.assign("NUM_ABNORMAL_EDGES_IN_CFG");
        return featureName;
    case NUM_INSTNS:
        featureName.assign("NUM_INSTNS");
        return featureName;
    case NUM_UNARY_INSTNS:
        featureName.assign("NUM_UNARY_INSTNS");
        return featureName;
    case NUM_BINARY_INT_OPS:
        featureName.assign("NUM_BINARY_INT_OPS");
        return featureName;
    case NUM_BINARY_FP_OPS:
        featureName.assign("NUM_BINARY_FP_OPS");
        return featureName;
    case NUM_DIRECT_CALLS:
        featureName.assign("NUM_DIRECT_CALLS");
        return featureName;
    case NUM_INDIRECT_CALLS:
        featureName.assign("NUM_INDIRECT_CALLS");
        return featureName;
    case NUM_CONDITIONAL_BRANCHES:
        featureName.assign("NUM_CONDITIONAL_BRANCHES");
        return featureName;
    case NUM_UNCONDITIONAL_BRANCHES:
        featureName.assign("NUM_UNCONDITIONAL_BRANCHES");
        return featureName;
    case NUM_SWITCH_INSTNS:
        featureName.assign("NUM_SWITCH_INSTNS");
        return featureName;
    case NUM_POINTER_ARITH_INSTNS:
        featureName.assign("NUM_POINTER_ARITH_INSTNS");
        return featureName;
    case NUM_PHI_NODES:
        featureName.assign("NUM_PHI_NODES");
        return featureName;
    case NUM_AVG_INSTNS_PER_BB:
        featureName.assign("NUM_AVG_INSTNS_PER_BB");
        return featureName;
    case NUM_AVG_PHI_NODES_PER_BB:
        featureName.assign("NUM_AVG_PHI_NODES_PER_BB");
        return featureName;
    case NUM_AVG_ARGS_PER_PHI_NODE:
        featureName.assign("NUM_AVG_ARGS_PER_PHI_NODE");
        return featureName;
    case NUM_BINARY_OPS_WITH_ONE_OP_CONST:
        featureName.assign("NUM_BINARY_OPS_WITH_ONE_OP_CONST");
        return featureName;
    case NUM_FUNC_CALLS_WITH_POINTER_ARGS:
        featureName.assign("NUM_FUNC_CALLS_WITH_POINTER_ARGS");
        return featureName;
    case NUM_FUNC_CALLS_WITH_MORE_THAN_4_ARGS:
        featureName.assign("NUM_FUNC_CALLS_WITH_MORE_THAN_4_ARGS");
        return featureName;
    case NUM_FUNC_CALLS_RETURNING_POINTER:
        featureName.assign("NUM_FUNC_CALLS_RETURNING_POINTER");
        return featureName;
    case NUM_FUNC_CALLS_RETURNING_INT:
        featureName.assign("NUM_FUNC_CALLS_RETURNING_INT");
        return featureName;
    case NUM_OCCURENCES_OF_0:
        featureName.assign("NUM_OCCURENCES_OF_0");
        return featureName;
    case NUM_OCCURENCES_OF_1:
        featureName.assign("NUM_OCCURENCES_OF_1");
        return featureName;
    case NUM_OCCURENCES_OF_INT:
        featureName.assign("NUM_OCCURENCES_OF_INT");
        return featureName;
    default:
        featureName.assign("UNKNOWN FEATURE");
        return featureName;
    }
}

#endif // __FEATURES_H
