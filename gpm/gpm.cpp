// {{{ Includes
/* For PrettyStackTraceOnErrorSignal */
#include "llvm/Support/PrettyStackTrace.h"
/* For llvm_shutdown_obj */
#include "llvm/Support/ManagedStatic.h"
/* For ParseIRFile  */
#include "llvm/Support/IRReader.h"
/* For tool_output_file */
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/ToolOutputFile.h"

/* For target  */
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetData.h"
#include "llvm/Target/TargetLibraryInfo.h"

/* For target */
#include "llvm/ADT/Triple.h"
/* For OwningPtr */
#include "llvm/ADT/OwningPtr.h"

/* For the passes */
#include "llvm/LinkAllPasses.h"
/* For createPrintModulePass */
#include "llvm/LinkAllVMCore.h"

/* For LLVMContext and getGlobalContext() */
/* Also for SMDiagnostic */
#include "llvm/LLVMContext.h"
/* For PassManager */
#include "llvm/PassManager.h"
/* For PassKind, Pass and PassInfo  */
#include "llvm/Pass.h"
#include "llvm/PassSupport.h"
/* For PassRegistry */
#include "llvm/PassRegistry.h"
/* For Module */
#include "llvm/Module.h"

/* Standard Includes */
#include <string>
#include <vector>
#include <cassert>

#include "include/gpm.hpp"
// }}}

GPM::GPM(std::string inFileName, std::string outFileName)
  :inFileName_(inFileName),
   outFileName_(outFileName)
{  
  /* How do I register user defined passes? 
     Got to look into opt's -load */
  Registry_ = llvm::PassRegistry::getPassRegistry();
     
  Context_ = &(llvm::getGlobalContext());
  initializeCore(*Registry_);
  initializeScalarOpts(*Registry_);
  initializeIPO(*Registry_);
  initializeAnalysis(*Registry_);
  initializeIPA(*Registry_);
  initializeTransformUtils(*Registry_);
  initializeInstCombine(*Registry_);
  initializeInstrumentation(*Registry_);
  initializeTarget(*Registry_);

  
  llvm::SMDiagnostic Err;  
  M.reset(ParseIRFile(inFileName, Err, *Context_));  
  if(M.get() == 0){
    Err.Print("gpm", llvm::errs());          
  }
  
  std::string ErrorInfo;

  Out_.reset(new llvm::tool_output_file(outFileName.c_str(), ErrorInfo,
				       llvm::raw_fd_ostream::F_Binary));
  
  if(!ErrorInfo.empty()){
    llvm::errs() << ErrorInfo << '\n';
  }
  
  /* std::auto_ptr<llvm::TargetMachine> target */;
  const std::string &ModuleDataLayout = M.get()->getDataLayout();

  /* TargetData isn't needed now, but it might be. So, I have it. */
  if (!ModuleDataLayout.empty()) {
    TD_ = new llvm::TargetData(ModuleDataLayout);
    /* No support for DefaultDataLayout like opt */
  }
  else {
    llvm::errs() << "Warning: ModuleLayout not found\n";
  }  
  
  /* llvm::TargetLibraryInfo *TLI = new llvm::TargetLibraryInfo(llvm::Triple(M->getTargetTriple())); */

  gpmprl_ = new GPMPassRegistrationListener();
  gpmprl_.generatePassNames();
  
}

bool GPM::runForPasses(std::vector<std::string> passNames)
{  
  Passes_ = new llvm::PassManager();
  /* Create a PassList from names */
  std::vector<const llvm::PassInfo*> PassList;
  for(std::vector<std::string>::iterator passNameIter = passNames.begin(); passNameIter != passNames.end(); passNameIter++){  
    /* Could optimize this by caching passNames<->passInfo mapping in
       GPMPassRegistrationListener */
    const llvm::PassInfo *P;
    if((P=gpmprl_->getPassInfoForName(*passNameIter)) == NULL){
      llvm::errs() << "Pass name not recognized : " << *passNameIter << "\n"; 
      return false;
    }
    llvm::errs()<<"adding pass " << *passNameIter << "\n";
    PassList.push_back(P);
  }
  
  /* And run them...  */
  for(unsigned i = 0; i < PassList.size(); i++){
    const llvm::PassInfo *PassInf = PassList[i];
    llvm::Pass *P = 0;
    if (PassInf->getNormalCtor())
      P = PassInf->getNormalCtor()();
    else
      llvm::errs() <<  "gpm: cannot create pass: "
	     << PassInf->getPassName() << "\n";
    if (P) {
      /* llvm::PassKind Kind = P->getPassKind(); */
      Passes_->add(P);
    }
  }
  Passes_->run(*M.get()); 
  /* Passes_.add(createVerifierPass()); */
  delete Passes_;
  return true;
}

void GPM::dumpToFile(){
  Passes_ = new llvm::PassManager();
  Passes_->add(llvm::createBitcodeWriterPass(Out_->os()));
  Passes_->run(*M.get());
  Out_->keep();
  delete Passes_;
}
