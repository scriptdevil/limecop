#include <vector>
#include <string>
#include "gpm.hpp"  
  
int main(int argc, char *argv[])
{
  std::string inFile(argv[1]), outFile(argv[2]);
  GPM gloryglory(inFile, outFile);
  std::vector<std::string> v;
  
  v.push_back("reg2mem");
  v.push_back("licm");
  gloryglory.runForPasses(v);
  gloryglory.dumpToFile();
  return 0;
}
